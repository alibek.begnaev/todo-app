import { Home } from "@material-ui/icons";
import NavBar from "../../components/NavBar/NavBar";
import TodoList from "../../components/TodoList/TodoList";
function HomePage() {
  return (
    <div className="App">
      <NavBar />
      <TodoList />
    </div>
  );
}

export default HomePage;
