import React, { useEffect } from "react";
import clsx from "clsx";
import { connect } from "react-redux";
import { useForm } from "react-hook-form";
import { loginWithUsernameAndPasswordAction } from "../../redux/actions/LoginActions";
import CssBaseline from "@material-ui/core/CssBaseline";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import {
  Container,
  TextField,
  Card,
  CardContent,
  Button,
  IconButton,
  FormControl,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from "@material-ui/core";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import { Visibility, VisibilityOff } from "@material-ui/icons";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      marginTop: "70%",
    },
    margin: {
      margin: theme.spacing(1),
    },
    textField: {
      width: "95%",
    },
  })
);

const LoginPage = function (props) {
  const { signIn, user, createTemporaryPass } = props;

  const classes = useStyles();

  const { register, handleSubmit, errors } = useForm();
  const [open, setOpen] = React.useState(false);
  const [credentials, setCredentials] = React.useState();
  const [tempPassword, setTempPassword] = React.useState();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [showPassword, setShowPassword] = React.useState(false);

  const onSubmit = (data) => {
    createTemporaryPass(data);
    setCredentials(data);
    handleClickOpen();
  };

  const onSubmit1 = (e) => {
    console.log(e);

    signIn(e);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {}, []);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Card className={classes.root}>
        <CardContent>
          <form onSubmit={handleSubmit(onSubmit1)}>
            <TextField
              className={clsx(classes.margin, classes.textField)}
              fullWidth
              name="username"
              margin="normal"
              inputRef={register({ required: true })}
              id="outlined"
              label={"Username"}
              type="text"
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <AccountCircleOutlinedIcon />
                  </InputAdornment>
                ),
              }}
            />
            {errors.username && (
              <Typography component="span" color="error">
                {"Please fill the form"}
              </Typography>
            )}
            <FormControl
              className={clsx(classes.margin, classes.textField)}
              variant="outlined"
            >
              <InputLabel htmlFor="outlined-adornment-password">
                {"Password"}
              </InputLabel>
              <OutlinedInput
                name="password"
                inputRef={register({ required: true })}
                id="outlined-adornment-password"
                type={showPassword ? "text" : "password"}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
                labelWidth={70}
              />
            </FormControl>
            {errors.password && (
              <Typography component="span" color="error">
                {"Please fill the form"}
              </Typography>
            )}
            <Button
              className={clsx(classes.margin, classes.textField)}
              variant="contained"
              color="primary"
              type="submit"
            >
              Sign In
            </Button>
          </form>
        </CardContent>
      </Card>
    </Container>
  );
};

const mapDispatchToProps = (dispatch) => ({
  signIn: (credentials) =>
    dispatch(loginWithUsernameAndPasswordAction(credentials)),
});
const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
