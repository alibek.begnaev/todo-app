// import { SET_SPIN } from '../actions/SpinAction';

const SET_SPIN = "SET_SPIN";

const initialState = {};

const spinReducer = function (state = initialState, action) {
  switch (action.type) {
    case SET_SPIN: {
      return {
        ...state,
        isLoading: action.data,
      };
    }
    default: {
      return state;
    }
  }
};

export default spinReducer;
