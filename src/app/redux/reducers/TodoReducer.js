import {
  SET_TODO_DATA,
  REMOVE_TODO_DATA,
  GET_TODO_LIST,
  TODO_LOGGED_OUT,
} from "../actions/TodoActions";

const initialState = {
  todos: [],
};

const TodoReducer = function (state = initialState, action) {
  switch (action.type) {
    case SET_TODO_DATA: {
      return {
        ...state,
        currentUser: action.data,
      };
    }

    case REMOVE_TODO_DATA: {
      return {
        ...state,
      };
    }
    case TODO_LOGGED_OUT: {
      return state;
    }

    case GET_TODO_LIST: {
      return { ...state, todos: action.payload };
    }
    default: {
      return state;
    }
  }
};

export default TodoReducer;
