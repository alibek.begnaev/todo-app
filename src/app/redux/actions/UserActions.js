// import history from "history.js";
import jwtAuthService from "../../services/jwtAuthService";
import instance from "../../services/axios";
import { setError, setSuccess } from "./setError";
// import { setSpin } from "./SpinAction";
export const SET_USER_DATA = "USER_SET_DATA";
export const REMOVE_USER_DATA = "USER_REMOVE_DATA";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";

function userFilter(role) {
  return {
    SA: role === "SA",
    Manager: role === "Manager",
    Operator: role === "Operator",
  };
}
export function setUserData(user) {
  return (dispatch) => {
    dispatch({
      type: SET_USER_DATA,
      data: user,
    });
  };
}

export function logoutUser() {
  return (dispatch) => {
    jwtAuthService.logout();

    // history.push({
    //   pathname: "/login",
    // });

    dispatch({
      type: USER_LOGGED_OUT,
    });
  };
}
