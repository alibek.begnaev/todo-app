import instance from "./axios";
import localStorageService from "./localStorageService";
import { createBrowserHistory } from "history";
export const history = createBrowserHistory();

class JwtAuthService {
  loginWithUsernameAndPassword = (username, password) => {
    return new Promise(async (resolve, reject) => {
      var user = await instance.post("/v2/login?developer=Alibek", {
        username,
        password,
      });

      resolve({ ...user.data, user: { username } });
    }).then((data) => {
      console.log(data);
      this.setUser(data.user);
      setTimeout(() => {
        history.push("/");

        window.location.reload();
      }, 10);

      return data;
    });
  };

  logout = () => {
    this.setSession(null);
    this.removeUser();
  };

  setSession = (token) => {
    if (token) {
      localStorageService.setItem("jwt_token", token);

      instance.defaults.headers.common["Authorization"] = "Bearer " + token;
    } else {
      localStorageService.removeItem("jwt_token");
      delete instance.defaults.headers.common["Authorization"];
    }
  };

  setUser = (user) => {
    localStorageService.setItem("auth_user", user);
  };

  removeUser = () => {
    localStorageService.removeItem("auth_user");
  };
}

export default new JwtAuthService();
