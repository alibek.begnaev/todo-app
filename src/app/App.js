import HomePage from "./pages/Home";
import LoginPage from "./pages/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { history } from "./services/jwtAuthService";
function App() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route path="/login">
          <LoginPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
